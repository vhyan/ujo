<?php
session_start();
if($_SESSION['username']){
include "koneksi.php";
include "header.php";
include "navigasi.php";
include "footer.php";
?>
<section id="content"> 
<section class="vbox"> 
<section class="scrollable padder"> 
<div class="m-b-md"> 
<h3 class="m-b-none">SMAN 7 Mataram</h3> <small>Mendidik Untuk Maju</small> </div> 
<div class="doc-buttons"><a href="inputguru.php" class="btn btn-s-md btn-dark">+ Tambah Guru</a></div> <br>
<section class="panel panel-default"> 
<header class="panel-heading"> Data Guru </header> 
<div class="table-responsive"> 
<table class="table table-striped m-b-none" > 
<thead> <tr> 
<th width="5%">No</th> 
<th width="10%">Nip</th> 
<th width="20%">Nama</th> 
<th width="15%">Agama</th> 
<th width="15%">Alamat</th>
<th width="15%">Contact</th>
<th width="25%">Aksi</th>
</tr> </thead> 
<tbody>
<?php
	$a="select * from guru";
	$b=mysqli_query($koneksi,$a);
	$no=1;
	while($c=mysqli_fetch_array($b)){
	?>

<tr>
						<td><?php echo $no;?></td>
								<td><?php echo $c['Nip'];?></td>
								<td><?php echo $c['Nama_guru'];?></td>
								<td><?php echo $c['Agama'];?></td>
								<td><?php echo $c['Alamat'];?></td>
								<td><?php echo $c['No_hp'];?></td>
								<td>
								<a href="ubahguru.php?Nip=<?php echo $c['Nip'];?>"><i class="fa fa-edit"></i> Ubah</a> | 
								<a href="javascript:if(confirm('Anda yakin menghapus ini?'))
								{document.location='hapusguru.php?Nip=<?php echo $c['Nip']; ?>';}"><i class="fa fa-trash-o"></i> Hapus</a>
								</td>
								
</tr>
	  <?php $no++; } ?>


</tbody>
</table> </div> </section> 
</section> 
</section>
 </section>
 <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
 </a> 
 </section> 

<script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
<script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script> <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script> <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script> 
<script src="js/charts/flot/demo.js" cache="false"></script> 
<script src="js/calendar/bootstrap_calendar.js" cache="false"></script> 
<script src="js/calendar/demo.js" cache="false"></script> 
<script src="js/sortable/jquery.sortable.js" cache="false"></script>
<script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
<script src="js/fuelux/fuelux.js" cache="false"></script>
<script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script>
<script src="js/slider/bootstrap-slider.js" cache="false"></script>
<script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script>
<script src="js/libs/moment.min.js" cache="false"></script>
<script src="js/combodate/combodate.js" cache="false"></script>
<script src="js/select2/select2.min.js" cache="false"></script>
<script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
<script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
<script src="js/wysiwyg/demo.js" cache="false"></script>
<script src="js/markdown/epiceditor.min.js" cache="false"></script>
<script src="js/markdown/demo.js" cache="false"></script>
</body>
</html>
 <?php
 }else{
echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
 ?>
