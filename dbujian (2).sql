-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25 Mei 2019 pada 09.27
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbujian`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `Nip` varchar(20) NOT NULL,
  `Nama_guru` varchar(45) NOT NULL,
  `Tempat_lahir` varchar(35) NOT NULL,
  `Tanggal_lahir` varchar(30) NOT NULL,
  `JK` varchar(20) NOT NULL,
  `Agama` varchar(25) NOT NULL,
  `Alamat` varchar(100) NOT NULL,
  `No_hp` varchar(20) NOT NULL,
  `Foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`Nip`, `Nama_guru`, `Tempat_lahir`, `Tanggal_lahir`, `JK`, `Agama`, `Alamat`, `No_hp`, `Foto`) VALUES
('2020', 'Setiawati ', 'Mataram', '17-05-2016', 'Laki-Laki', 'Budha', 'Labuapi', '0832356477345', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
`id_kelas` int(11) NOT NULL,
  `id_nama_kelas` int(11) DEFAULT NULL,
  `nis` varchar(20) DEFAULT NULL,
  `status_ujian` enum('A','N') DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `id_nama_kelas`, `nis`, `status_ujian`) VALUES
(6, 4, '1333', 'A'),
(7, 4, '1212', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE IF NOT EXISTS `mapel` (
  `Kd_mapel` varchar(20) NOT NULL,
  `Nama_mapel` varchar(40) NOT NULL,
  `Jumlah_soal` int(5) NOT NULL,
  `Status` varchar(35) NOT NULL,
  `Ulang` int(11) NOT NULL,
  `Jam` int(11) NOT NULL,
  `Menit` int(11) NOT NULL,
  `Detik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`Kd_mapel`, `Nama_mapel`, `Jumlah_soal`, `Status`, `Ulang`, `Jam`, `Menit`, `Detik`) VALUES
('1234', 'Matematika', 4, 'Aktif', 2, 1, 1, 10),
('IP024', 'Fisika', 3, 'aktif', 3, 1, 30, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nama_kelas`
--

CREATE TABLE IF NOT EXISTS `nama_kelas` (
`id_nama_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nama_kelas`
--

INSERT INTO `nama_kelas` (`id_nama_kelas`, `nama_kelas`) VALUES
(1, 'X-A'),
(2, 'X-B'),
(3, 'X-C'),
(4, 'XI-A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `Nis` varchar(20) NOT NULL,
  `Nama` varchar(45) NOT NULL,
  `Tempat_lahir` varchar(20) NOT NULL,
  `Tanggal_lahir` varchar(30) NOT NULL,
  `JK` varchar(20) NOT NULL,
  `Agama` varchar(25) NOT NULL,
  `Alamat` varchar(100) NOT NULL,
  `No_hp` varchar(20) NOT NULL,
  `Foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`Nis`, `Nama`, `Tempat_lahir`, `Tanggal_lahir`, `JK`, `Agama`, `Alamat`, `No_hp`, `Foto`) VALUES
('1212', 'Rojib Cipto', 'Mataram', '20-12-1994', 'Laki-Laki', 'Islam', 'Jl. kalibaru No.31 Tinggar Ampenan Utara', '082340271507', 'aaaa.jpg'),
('1333', 'Angger  Dwipa', 'Senggigi', '11-05-2016', 'L', 'Budha', 'Sengigi', '0324832957934', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_ujian`
--

CREATE TABLE IF NOT EXISTS `status_ujian` (
`id_status` int(11) NOT NULL,
  `nis` varchar(20) DEFAULT NULL,
  `kd_mapel` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status_ujian`
--

INSERT INTO `status_ujian` (`id_status`, `nis`, `kd_mapel`) VALUES
(4, '1212', '1234');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbnilai`
--

CREATE TABLE IF NOT EXISTS `tbnilai` (
`Id` int(20) NOT NULL,
  `Kd_mapel` varchar(20) NOT NULL,
  `Nis` varchar(20) NOT NULL,
  `Salah` double(5,2) NOT NULL,
  `Benar` double(5,2) NOT NULL,
  `Jumlah_soal` int(5) NOT NULL,
  `Nilai` double NOT NULL,
  `Tanggal` date NOT NULL,
  `Ujian_ke` int(5) NOT NULL,
  `Jam` int(11) NOT NULL,
  `Menit` int(11) NOT NULL,
  `Detik` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbnilai`
--

INSERT INTO `tbnilai` (`Id`, `Kd_mapel`, `Nis`, `Salah`, `Benar`, `Jumlah_soal`, `Nilai`, `Tanggal`, `Ujian_ke`, `Jam`, `Menit`, `Detik`) VALUES
(86, '1234', '1212', 4.00, 0.00, 4, 0, '2019-05-20', 1, 0, 0, 4),
(87, '1234', '1212', 4.00, 0.00, 4, 0, '2019-05-20', 2, 0, 0, 31);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbsoal`
--

CREATE TABLE IF NOT EXISTS `tbsoal` (
`Id_soal` int(20) NOT NULL,
  `Kd_mapel` varchar(20) NOT NULL,
  `Soal` text NOT NULL,
  `Jawaban` text NOT NULL,
  `Pilihan1` text NOT NULL,
  `Pilihan2` text NOT NULL,
  `Pilihan3` text NOT NULL,
  `Pilihan4` text NOT NULL,
  `Format` int(5) NOT NULL,
  `jenis_soal` enum('G','E') DEFAULT 'G'
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbsoal`
--

INSERT INTO `tbsoal` (`Id_soal`, `Kd_mapel`, `Soal`, `Jawaban`, `Pilihan1`, `Pilihan2`, `Pilihan3`, `Pilihan4`, `Format`, `jenis_soal`) VALUES
(39, '1234', '1+1=', '2', '6', '8', '6', '5', 0, 'G'),
(41, '1234', '23+2', '25', '72', '28', '72', '24', 0, 'G'),
(44, '1234', '<p>Bagainan cara kamu meningkatkan kemampuan diri?</p>\r\n', 'up she up', '-', '-', '-', '-', 0, 'E'),
(45, '1234', '<p>Sebutkan salah satu langkah untuk mendapatkan kemampuan meningkatkan diri</p>\r\n', 'banyak mencari pengalaman', '-', '-', '-', '-', 0, 'E'),
(46, '1234', '<p><img alt="" src="/ujo/plug-ins/kcfinder/upload/images/243f4251894ffa998fe0b51acadd3ac0.jpg" style="border-style:solid; border-width:2px; height:100px; margin:2px; width:100px" /></p>\r\n\r\n<p>sebutkan komponen yang akan anda gunakan</p>\r\n', 'makan malam', '-', '-', '-', '-', 0, 'E');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbtugas`
--

CREATE TABLE IF NOT EXISTS `tbtugas` (
`Id_tugas` int(20) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `Judul_tugas` varchar(100) NOT NULL,
  `File` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbtugas`
--

INSERT INTO `tbtugas` (`Id_tugas`, `nip`, `Judul_tugas`, `File`) VALUES
(22, '2020', 'djsd', 'TUGAS PENGOLAHAN.docx');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `Nama` varchar(50) NOT NULL,
  `Username` varchar(25) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Foto` varchar(100) NOT NULL,
  `Level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`Nama`, `Username`, `Password`, `Foto`, `Level`) VALUES
('Rojib Cipto', '1212', '01a5f5db2d97bd6b389e7a20bd889708', '', 'siswa'),
('Angger  Dwipa', '1333', 'ff49cc40a8890e6a60f40ff3026d2730', '', 'siswa'),
('Budi Harto', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'avatar1.jpg', 'admin'),
('Setiawati ', 'guru', '01a5f5db2d97bd6b389e7a20bd889708', '', 'guru');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
 ADD PRIMARY KEY (`Nip`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
 ADD PRIMARY KEY (`id_kelas`), ADD KEY `id_nama_kelas` (`id_nama_kelas`), ADD KEY `nis` (`nis`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
 ADD PRIMARY KEY (`Kd_mapel`);

--
-- Indexes for table `nama_kelas`
--
ALTER TABLE `nama_kelas`
 ADD PRIMARY KEY (`id_nama_kelas`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
 ADD PRIMARY KEY (`Nis`);

--
-- Indexes for table `status_ujian`
--
ALTER TABLE `status_ujian`
 ADD PRIMARY KEY (`id_status`), ADD KEY `nis` (`nis`), ADD KEY `kd_mapel` (`kd_mapel`);

--
-- Indexes for table `tbnilai`
--
ALTER TABLE `tbnilai`
 ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbsoal`
--
ALTER TABLE `tbsoal`
 ADD PRIMARY KEY (`Id_soal`);

--
-- Indexes for table `tbtugas`
--
ALTER TABLE `tbtugas`
 ADD PRIMARY KEY (`Id_tugas`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `nama_kelas`
--
ALTER TABLE `nama_kelas`
MODIFY `id_nama_kelas` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `status_ujian`
--
ALTER TABLE `status_ujian`
MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbnilai`
--
ALTER TABLE `tbnilai`
MODIFY `Id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `tbsoal`
--
ALTER TABLE `tbsoal`
MODIFY `Id_soal` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tbtugas`
--
ALTER TABLE `tbtugas`
MODIFY `Id_tugas` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kelas`
--
ALTER TABLE `kelas`
ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`id_nama_kelas`) REFERENCES `nama_kelas` (`id_nama_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `kelas_ibfk_2` FOREIGN KEY (`nis`) REFERENCES `siswa` (`Nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `status_ujian`
--
ALTER TABLE `status_ujian`
ADD CONSTRAINT `status_ujian_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`Nis`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `status_ujian_ibfk_2` FOREIGN KEY (`kd_mapel`) REFERENCES `mapel` (`Kd_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
