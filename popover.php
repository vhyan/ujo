<?php
include 'koneksi.php';
$id = $_GET['id'];
$query = mysqli_query($koneksi, "SELECT * FROM status_ujian JOIN mapel ON mapel.Kd_mapel=status_ujian.kd_mapel WHERE nis='$id'");
$query_mapel = mysqli_query($koneksi, "SELECT * FROM mapel WHERE Kd_mapel NOT IN (SELECT kd_mapel FROM status_ujian WHERE nis='$id')");
$count = mysqli_num_rows($query);
?>

<form role="form" method="post" action="status_ujian.php">
    <div class="form-group">
        <label>Aktifkan Pelajaran</label>
        <div class="input-group">
            <input type="hidden" name="nis" value="<?= $id ?>">
            <select name="kd_mapel" class="form-control">
                <option value="" selected disabled>Pilih</option>
                <?php while ($row = mysqli_fetch_array($query_mapel)) : ?>
                    <option value="<?= $row['Kd_mapel'] ?>"><?= $row['Nama_mapel'] ?></option>
                <?php endwhile; ?>
            </select>
            <div class="input-group-btn">
                <input type="submit" name="submit" value="OK" class="btn btn-danger" tabindex="-1"/>
            </div>
        </div>
    </div>
</form>
<?php if ($count > 0) : ?>
<hr>
<table class="table table-striped m-b-none text-sm">
    <thead>
        <tr>
            <th>Pelajaran</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php while ($row = mysqli_fetch_array($query)) : ?>
        <tr>
            <td><?= $row['Nama_mapel'] ?></td>
            <td class="text-danger">
                <a href="status_ujian.php?id=<?= $row['id_status'] ?>">
                    <span class="text-danger"><i class="fa fa-times"></i></span>
                </a>
            </td>
        </tr>
        <?php endwhile; ?>
    </tbody>
</table>
<?php endif; ?>
