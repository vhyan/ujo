<?php
session_start();
if(isset($_SESSION['username']) && isset($_SESSION['level'])){
include "koneksi.php";
include "header.php";
include "navigasi.php";
include "footer.php";
?>

<section id="content"> 
<section class="vbox"> 
<section class="scrollable padder"> 
<div class="m-b-md"> 
<h3 class="m-b-none">SMAN 7 Mataram</h3> <small>Mendidik Untuk Maju</small> </div> 
<div class="col-sm-8"> 
<section class="panel panel-default"> 
<header class="panel-heading font-bold">Input Data Tugas</header> 
<div class="panel-body">

<?php
if(isset($_POST['simpan'])){
$nama=$_SESSION['username'];
$judul=$_POST['judul'];
$lokasi_file= $_FILES['upload']['tmp_name'];
$nama_file=$_FILES['upload']['name'];
$folder= "files/$nama_file";
if(move_uploaded_file($lokasi_file,"$folder")){
if(empty($judul) || empty($nama_file)){
echo "<script language='javascript'>
alert('Data belum lengkap');
</script>"; 
}else{
$sql="insert into tbtugas values('','$nama','$judul','$nama_file')";
$query=mysqli_query($koneksi,$sql) ;
		if ($query) {
		echo"<script language='javascript'>
alert('Data berhasil disimpan');
</script>";
		}
}
}
}

?>
<form class="bs-example form-horizontal" method="post" action="" enctype="multipart/form-data"> 
 
<div class="form-group"> 
<label class="col-lg-2 control-label">Judul Tugas</label> <div class="col-lg-10"> 
<input type="text" name="judul" class="form-control" placeholder="Masukkan judul tugas" autofocus> </div> </div> 

<div class="form-group"> <label class="col-sm-2 control-label">File Tugas</label> 
<div class="col-sm-10"> 
<input type="file" name="upload" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
</div> </div>

<br>
<a href="tugas.php"><input type="button" class="btn btn-default" value="Cancel"></input></a> 
<button type="submit" name="simpan" class="btn btn-primary">Save Change</button>

</form> 
</div> </section> 

<section class="panel panel-default"> 
<header class="panel-heading"> Data Tugas </header> 
<div class="table-responsive"> 
<table class="table table-striped m-b-none" > 
<thead> <tr> 
<th width="5%">No</th> 
<th width="5%">Id</th> 
<th width="25%">Judul Tugas</th> 
<th width="25%">File</th> 
<th width="25%">Aksi</th>
</tr> </thead> 
<tbody>
<?php
	$a="select * from tbtugas";
	$b=mysqli_query($koneksi,$a);
	$no=1;
	while($c=mysqli_fetch_array($b)){
	?>

<tr>
								<td><?php echo $no;?></td>
								<td><?php echo $c['Id_tugas'];?></td>
								<td><?php echo $c['Judul_tugas'];?></td>
								<td><?php echo $c['File'];?></td>
								
								<td>
								<a href="javascript:if(confirm('Anda yakin menghapus ini?'))
								{document.location='hapustugas.php?Id_tugas=<?php echo $c['Id_tugas']; ?>';}"><i class="fa fa-trash-o"></i> Hapus</a>
								</td>
								
</tr>
	  <?php $no++; } ?>


</tbody>
</table> </div> </section> 



</div>
 </section>
 </section> 
 </section> 
 
 
<script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
<script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script> <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script> <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script> 
<script src="js/charts/flot/demo.js" cache="false"></script> 
<script src="js/calendar/bootstrap_calendar.js" cache="false"></script> 
<script src="js/calendar/demo.js" cache="false"></script> 
<script src="js/sortable/jquery.sortable.js" cache="false"></script>
<script src="js/fuelux/fuelux.js" cache="false"></script><!-- datepicker --><script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script><!-- slider --><script src="js/slider/bootstrap-slider.js" cache="false"></script><!-- file input --> <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script><!-- combodate --><script src="js/libs/moment.min.js" cache="false"></script><script src="js/combodate/combodate.js" cache="false"></script><!-- select2 --><script src="js/select2/select2.min.js" cache="false"></script><!-- wysiwyg --><script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script><script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script><script src="js/wysiwyg/demo.js" cache="false"></script><!-- markdown --><script src="js/markdown/epiceditor.min.js" cache="false"></script><script src="js/markdown/demo.js" cache="false"></script>
</body>
</html>
<?php
	}else{
echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
 ?>
