<?php
session_start();
if (isset($_SESSION['username']) && isset($_SESSION['level'])) {
    include "koneksi.php";
    include "header.php";
    include "navigasi.php";
    include "footer.php";
    ?>

    <section id="content">
        <section class="vbox">
            <section class="scrollable padder">
                <div class="m-b-md">
                    <h3 class="m-b-none">SMAN 7 Mataram</h3>
                    <small>Mendidik Untuk Maju</small>
                </div>
                <div class="col-sm-10">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Input Data Soal</header>
                        <div class="panel-body">
                            <?php
                            if (isset($_POST['simpan'])) {
                                $kd_mapel = $_POST['kd_mapel'];
                                $soal = $_POST['text-ckeditor'];
                                $jawaban = $_POST['jawaban'];
                                $jenis_soal = $_POST['jenis_soal'];
                                $pilihan1 = $_POST['pilihan1'];
                                $pilihan2 = $_POST['pilihan2'];
                                $pilihan3 = $_POST['pilihan3'];
                                $pilihan4 = $_POST['pilihan4'];
                                if ($jenis_soal == 'G') {
                                    if (empty($kd_mapel) || empty($soal) || empty($jawaban) || empty($pilihan1) || empty($pilihan2) || empty($pilihan3) || empty($pilihan4)) {
                                        echo "<script language='javascript'>
        alert('Data belum lengkap');
        </script>";
                                    } else {
                                        $sql = "insert into tbsoal values('','$kd_mapel','$soal','$jawaban','$pilihan1','$pilihan2','$pilihan3','$pilihan4')";
                                        $query = mysqli_query($koneksi, $sql) or die(mysql_error());
                                        if ($query) {
                                            echo "<script language='javascript'>
            alert('Data berhasil disimpan');
            </script>";
                                        }
                                    }
                                } else {
                                    if (empty($kd_mapel) || empty($soal) || empty($jawaban)) {
                                        echo "<script language='javascript'>
        alert('Data belum lengkap');
        </script>";
                                    } else {
                                        $sql = "insert into tbsoal values('','$kd_mapel','$soal','$jawaban','-','-','-','-','-','$jenis_soal')";
                                        $query = mysqli_query($koneksi, $sql) or die(mysql_error());
                                        if ($query) {
                                            echo "<script language='javascript'>
            alert('Data berhasil disimpan');
            </script>";
                                        }
                                    }
                                }

                            }
                            ?>
                            <form class="bs-example form-horizontal" method="post" action=""
                                  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Mapel</label>
                                    <div class="col-lg-10">

                                        <select class="input-sm input-s  form-control" name="kd_mapel">
                                            <?php
                                            $mapel = "select * from mapel";
                                            $query = mysqli_query($koneksi, $mapel);
                                            if ($query === FALSE) {
                                                die (mysql_error());
                                            }
                                            while ($a = mysqli_fetch_array($query)) {
                                                echo "<option value='$a[Kd_mapel]'>$a[Nama_mapel]</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group"><label class="col-sm-2 control-label">Soal</label>
                                    <div class="col-sm-10">
                                        <textarea name="text-ckeditor" id="text-ckeditor" class="form-control m-b"
                                                  placeholder="Soal" rows="3"></textarea>
                                        <script>
                                            CKEDITOR.replace('text-ckeditor');
                                        </script>
                                    </div>
                                </div>

                                <!--            jenis soal-->
                                <div class="form-group"><label class="col-sm-2 control-label">Jenis So'al</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="jenis_soal" id="jenis_soal">
                                            <option value="G" selected>PILIHAN GANDA</option>
                                            <option value="E">ESAY</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-sm-2 control-label">Jawaban</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control m-b" name="jawaban" placeholder="Jawaban"
                                                  rows="3"></textarea>
                                    </div>
                                </div>

                                <div id="pilihan">
                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 1</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control m-b" name="pilihan1" placeholder="pilihan 1"
                                                      rows="3"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 2</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control m-b" name="pilihan2" placeholder="pilihan 2"
                                                      rows="3"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 3</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control m-b" name="pilihan3" placeholder="pilihan 3"
                                                      rows="3"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 4</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control m-b" name="pilihan4" placeholder="pilihan 4"
                                                      rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <a href='datasoal.php'><input type='button' class='btn btn-default'
                                                              value='Cancel'></input></a>
                                <button type='submit' name='simpan' class='btn btn-primary'>Save Change</button>

                            </form>
                        </div>
                    </section>
                </div>
            </section>
        </section>
    </section>

    <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App -->
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="js/charts/flot/demo.js" cache="false"></script>
    <script src="js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="js/calendar/demo.js" cache="false"></script>
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/fuelux/fuelux.js" cache="false"></script><!-- datepicker -->
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script><!-- slider -->
    <script src="js/slider/bootstrap-slider.js" cache="false"></script><!-- file input -->
    <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script><!-- combodate -->
    <script src="js/libs/moment.min.js" cache="false"></script>
    <script src="js/combodate/combodate.js" cache="false"></script><!-- select2 -->
    <script src="js/select2/select2.min.js" cache="false"></script><!-- wysiwyg -->
    <script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
    <script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
    <script src="js/wysiwyg/demo.js" cache="false"></script><!-- markdown -->
    <script src="js/markdown/epiceditor.min.js" cache="false"></script>
    <script src="js/markdown/demo.js" cache="false"></script>
    <script>
        $(document).ready(function () {
            $('#jenis_soal').change(function () {
                var jenis_soal = $(this).val();
                if (jenis_soal == 'E') {
                    $('#pilihan').hide();
                } else {
                    $('#pilihan').show();
                }
            })
        })
    </script>
    </body>
    </html>
    <?php
} else {
    echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
?>

