<?php
session_start();
if(isset($_SESSION['username']) && isset($_SESSION['level'])){
include "koneksi.php";
include "header.php";
include "navigasi.php";
include "footer.php";
if(isset($_GET['Nis'])){
$nis=$_GET['Nis'];
$sql="select * from siswa where Nis='$nis'";
$query=mysqli_query($koneksi,$sql);
$data=mysqli_fetch_array($query);
}else{
echo "Data yang diubah belum ada";
}
?>
<section id="content"> 
<section class="vbox"> 
<section class="scrollable padder"> 
<div class="m-b-md"> 
<h3 class="m-b-none">SMAN 7 Mataram</h3><small>Mendidik Untuk Maju</small> </div> 
<div class="col-sm-8"> 
<section class="panel panel-default"> 
<header class="panel-heading font-bold">Ubah Data Siswa</header> 
<div class="panel-body"> 

<?php
if(isset($_POST['ubah'])){
$nise=md5($_GET['Nis']);
$nama=$_POST['nama'];
$tlahir=$_POST['tlahir'];
$tgllahir=$_POST['tgllahir'];
$jk=$_POST['jk'];
$agama=$_POST['agama'];
$alamat=$_POST['alamat'];
$nohp=$_POST['nohp'];
$foto=$_FILES['foto']['name'];
		if(strlen($foto)){
			if(is_uploaded_file($_FILES['foto']['name'])){
				move_uploaded_file($_FILES['foto']['name'],"images/".$foto);
			}
			mysqli_query($koneksi,"update siswa set Foto='$foto' where Nis='$_GET[Nis]'");
		}
		if (empty($nama) or empty($tlahir)or empty($tgllahir)or empty($jk)or empty($agama)or empty($alamat)or empty($nohp)){
			echo "<script language='javascript'>
alert('Data belum lengkap !');
</script>";
		}else{
		$a="update siswa set Nama='$nama', Tempat_lahir='$tlahir', Tanggal_lahir ='$tgllahir', JK='$jk',Agama='$agama',  Alamat='$alamat', No_hp='$nohp' where Nis='$nis'";
		$x="update user set Nama='$nama', Password='$nise',Foto='$foto' where Username='$nis'";
		$y=mysqli_query($koneksi,$x);
		$b=mysqli_query($koneksi,$a);
		move_uploaded_file($_FILES['foto']['tmp_name'], "images/".$_FILES['foto']['name']);
			if ($b) {
				echo "<script language='javascript'>
alert('Ubah data berhasil');
document.location='datasiswa.php';
</script>";

			}
		}
	}
?>
<form class="bs-example form-horizontal" method="post" action="" enctype="multipart/form-data"> 
<div class="form-group"> 
<label class="col-lg-2 control-label">NIS</label> <div class="col-lg-10"> <input type="text" name="nis" class="form-control" value="<?php echo $data['Nis'];?>" disabled> </div> </div> 
<div class="form-group"> 
<label class="col-lg-2 control-label">Nama</label> <div class="col-lg-10"> <input type="text" name="nama" class="form-control" value="<?php echo $data['Nama'];?>" > </div> </div>
<div class="form-group">
<label class="col-lg-2 control-label">Tempat Lahir</label> <div class="col-lg-10"> <input type="text" name="tlahir" class="form-control" value="<?php echo $data['Tempat_lahir'];?>"> </div> </div> 
<div class="form-group">
<label class="col-sm-2 control-label">Tanggal Lahir</label> <div class="col-lg-10"> <input name="tgllahir" value="<?php echo $data['Tanggal_lahir'];?>"  class="input-sm input-s datepicker-input form-control" size="16" type="text" value="" data-date-format="dd-mm-yyyy" > </div></div>
<div class="form-group"> 
<label class="col-sm-2 control-label">Jenis Kelamin</label>
<div class="col-sm-10"> 
<select class="input-sm input-s form-control" name="jk" value="<?php echo $data['JK'];?>" >
<option value="Laki-Laki" <?php if ($data['JK']=="Laki-Laki"){echo "selected";}?>>Laki-Laki</option> 
<option value="Perempuan" <?php if ($data['JK']=="Perempuan"){echo "selected";}?>>Perempuan</option> 
</select> </div> </div>
<div class="form-group"> <label class="col-sm-2 control-label">Agama</label>
<div class="col-sm-10"> 
<select class="input-sm input-s form-control" name="agama" value="<?php echo $data['Agama'];?>" >
<option value="Islam" <?php if ($data['Agama']=="Islam"){echo "selected";}?> >Islam</option> 
<option value="Hindu" <?php if ($data['Agama']=="Hindu"){echo "selected";}?>>Hindu</option>
<option value="Budha" <?php if ($data['Agama']=="Budha"){echo "selected";}?>>Budha</option> 
<option value="Kristen" <?php if ($data['Agama']=="Kristen"){echo "selected";}?>>Kristen</option> 
</select> </div> </div>
<div class="form-group"> <label class="col-sm-2 control-label">Alamat</label>
<div class="col-sm-10">  
<textarea class="form-control m-b" name="alamat"  rows="3"  ><?php echo $data['Alamat'];?></textarea></div></div>
<div class="form-group">
<label class="col-lg-2 control-label">Nomer HP/Telpon</label> <div class="col-lg-10"> <input type="text" name="nohp"  value="<?php echo $data['No_hp'];?>" class="form-control" placeholder="Masukkan nomer hp/telpon"> </div> </div> 
<div class="form-group"> <label class="col-sm-2 control-label">Foto Profile</label> 
<div class="col-sm-10"> 
<input type="file" name="foto" accept="image/*" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s"  >
&nbsp;&nbsp;
<?php
				echo "<img src='images/".$data['Foto']."' width='100px' height='100px'/>";
?>
</div> </div>

<br>
<a href="datasiswa.php"><input type="button" class="btn btn-default" value="Cancel"></input></a> 
<button type="submit" name="ubah" class="btn btn-primary">Ubah</button>

</form> 
</div> </section> </div>
</section> 
</section> 
</section>
<script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
<script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script> <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script> <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script> 
<script src="js/charts/flot/demo.js" cache="false"></script> 
<script src="js/calendar/bootstrap_calendar.js" cache="false"></script> 
<script src="js/calendar/demo.js" cache="false"></script> 
<script src="js/sortable/jquery.sortable.js" cache="false"></script>
<script src="js/fuelux/fuelux.js" cache="false"></script><!-- datepicker --><script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script><!-- slider --><script src="js/slider/bootstrap-slider.js" cache="false"></script><!-- file input --> <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script><!-- combodate --><script src="js/libs/moment.min.js" cache="false"></script><script src="js/combodate/combodate.js" cache="false"></script><!-- select2 --><script src="js/select2/select2.min.js" cache="false"></script><!-- wysiwyg --><script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script><script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script><script src="js/wysiwyg/demo.js" cache="false"></script><!-- markdown --><script src="js/markdown/epiceditor.min.js" cache="false"></script><script src="js/markdown/demo.js" cache="false"></script>
</body>
</html>
<?php
	}else{
echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
	?>