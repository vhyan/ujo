<?php
session_start();
error_reporting(true);
if (isset($_SESSION['username']) && isset($_SESSION['level'])) {
    include "koneksi.php";
    include "header.php";
    include "navigasi.php";
    include "footer.php";
    if (isset($_GET['Kd_mapel'])) {
        $kd = $_GET['Kd_mapel'];
        $sql = "select * from mapel where Kd_mapel='$kd'";
        $query = mysqli_query($koneksi, $sql);
        $data = mysqli_fetch_array($query);
    } else {
        echo "Data yang diubah belum ada";
    }
    ?>

    <section id="content">
        <section class="vbox">
            <section class="scrollable padder">
                <div class="m-b-md">
                    <h3 class="m-b-none">SMAN 7 Mataram</h3>
                    <small>Mendidik Untuk Maju</small>
                </div>
                <div class="col-sm-8">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Ubah Data Mapel</header>
                        <div class="panel-body">

                            <?php
                            if (isset($_POST['ubah'])) {
                                $nama_mapel = $_POST['nama_mapel'];
                                $jmlh_soal = $_POST['jmlh_soal'];
                                $status_soal = $_POST['status_soal'];
                                $ulang = $_POST['ulang'];
                                $jam = $_POST['jam'];
                                $menit = $_POST['menit'];
                                $detik = $_POST['detik'];

                                if (empty($nama_mapel) || empty($jmlh_soal) || empty($status_soal) || empty($ulang) || $jam != "" || $menit != "" || $detik != "") {
                                    echo "<script language='javascript'>
alert('Data belum lengkap');
</script>";
                                } else {
                                    $sql = "UPDATE mapel SET Nama_mapel='$nama_mapel', Jumlah_soal='$jmlh_soal',Status='$status_soal', Ulang='$ulang',Jam='$jam',Menit='$menit',Detik='$detik' where Kd_mapel='$kd'";
                                    $que = mysqli_query($koneksi, $sql);
                                    if ($que) {
                                        echo "<script language='javascript'>
alert('Ubah Data Berhasil');
document.location='datamapel.php';
</script>";
                                    }
                                }
                            }

                            ?>
                            <form class="bs-example form-horizontal" method="post" action=""
                                  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Kode Mata pelajaran</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="kd_mapel" class="form-control"
                                               value="<?php echo $data['Kd_mapel']; ?>" disabled></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nama Mata pelajaran</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="nama_mapel" class="form-control"
                                               value="<?php echo $data['Nama_mapel']; ?>"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Jumlah Soal</label>
                                    <div class="col-lg-10">
                                        <input name="jmlh_soal" type="text" class="form-control"
                                               value="<?php echo $data['Jumlah_soal']; ?>"></div>
                                </div>

                                <div class="form-group"><label class="col-lg-2 control-label">Status Soal</label>
                                    <div class="col-lg-10">
                                        <input type='radio' name='status_soal'
                                               value='Aktif' <?php if ($data['Status'] == "Aktif") {
                                            echo "checked";
                                        } ?> >Aktif&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type='radio' name='status_soal'
                                               value='Tidak Aktif' <?php if ($data['Status'] == "Tidak Aktif") {
                                            echo "checked";
                                        } ?> >Tidak Aktif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Ulang</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="ulang" class="form-control"
                                               value="<?php echo $data['Ulang']; ?>"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Waktu</label>
                                    <div class="col-lg-10">
                                        <label><input name='jam' type='text' id='jam' size='10' class="form-control"
                                                      value="<?php echo $data['Jam']; ?>"/></label>&nbsp;&nbsp;Jam
                                        <label><input name='menit' class="form-control" type='text' id='menit' size='10'
                                                      value="<?php echo $data['Menit']; ?>"/></label>&nbsp;&nbsp;Menit
                                        <label><input name='detik' type='text' class="form-control" id='detik' size='10'
                                                      value="<?php echo $data['Detik']; ?>"/></label>
                                        &nbsp;&nbsp;Detik
                                    </div>
                                </div>
                                <br>
                                <a href="datamapel.php"><input type="button" class="btn btn-default"
                                                               value="Cancel"></input></a>
                                <button type="submit" name="ubah" class="btn btn-primary">Ubah</button>

                            </form>
                        </div>
                    </section>
                </div>
            </section>
        </section>
    </section>

    <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App -->
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="js/charts/flot/demo.js" cache="false"></script>
    <script src="js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="js/calendar/demo.js" cache="false"></script>
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/fuelux/fuelux.js" cache="false"></script><!-- datepicker -->
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script><!-- slider -->
    <script src="js/slider/bootstrap-slider.js" cache="false"></script><!-- file input -->
    <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script><!-- combodate -->
    <script src="js/libs/moment.min.js" cache="false"></script>
    <script src="js/combodate/combodate.js" cache="false"></script><!-- select2 -->
    <script src="js/select2/select2.min.js" cache="false"></script><!-- wysiwyg -->
    <script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
    <script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
    <script src="js/wysiwyg/demo.js" cache="false"></script><!-- markdown -->
    <script src="js/markdown/epiceditor.min.js" cache="false"></script>
    <script src="js/markdown/demo.js" cache="false"></script>
    </body>
    </html>
    <?php
} else {
    echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
?>
