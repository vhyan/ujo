<?php
session_start();
$nis = $_SESSION['username'];
if (isset($_SESSION['username']) && isset($_SESSION['level'])) {
    include "koneksi.php";
    include "header.php";
    include "navigasi.php";
    include "footer.php";
    $query = mysqli_query($koneksi,"SELECT * FROM kelas where nis='$nis'");
    $status_ujian = mysqli_fetch_array($query);
    ?>
    <section id="content">
        <section class="vbox">
            <section class="scrollable padder">
                <div class="m-b-md">
                    <h3 class="m-b-none">SMAN 7 Mataram</h3>
                    <small>Mendidik Untuk Maju</small>
                </div>
                <div class="col-lg-10">
                    <section class="panel panel-default">
                        <header class="panel-heading"> Data Mapel</header>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="20%">Nama Mapel</th>
                                    <th width="5%">Ulang</th>
                                    <th width="15%">Waktu</th>
                                    <th width="25%">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $a = "select * from mapel";
                                $b = mysqli_query($koneksi, $a);
                                $no = 1;
                                while ($c = mysqli_fetch_array($b)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $c['Nama_mapel']; ?></td>
                                        <td><?php echo $c['Ulang']; ?></td>
                                        <td><?php echo $c['Jam']; ?> : <?php echo $c['Menit']; ?>
                                            : <?php echo $c['Detik']; ?></td>
                                        <?php
                                            $que = mysqli_query($koneksi, "SELECT * FROM status_ujian WHERE nis='$nis' AND kd_mapel='".$c['Kd_mapel']."'");
                                            $jum = mysqli_num_rows($que);
                                        ?>
                                        <?php if ($jum > 0) : ?>
                                        <td>
                                            <a href="tampilsoal.php?Kd_mapel=<?php echo $c['Kd_mapel']; ?>">
                                                <i class="fa fa-book"></i> Kerjakan Ujian</a>
                                        </td>
                                        <?php else: ?>
                                            <td>
                                               <span class="text-danger"><i class="fa fa-times"></i> Ujian Belum Aktif</span>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                    <?php $no++;
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </section>
        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
    </a>
    </section>

    <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App -->
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="js/charts/flot/demo.js" cache="false"></script>
    <script src="js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="js/calendar/demo.js" cache="false"></script>
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script src="js/fuelux/fuelux.js" cache="false"></script>
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script>
    <script src="js/slider/bootstrap-slider.js" cache="false"></script>
    <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script>
    <script src="js/libs/moment.min.js" cache="false"></script>
    <script src="js/combodate/combodate.js" cache="false"></script>
    <script src="js/select2/select2.min.js" cache="false"></script>
    <script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
    <script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
    <script src="js/wysiwyg/demo.js" cache="false"></script>
    <script src="js/markdown/epiceditor.min.js" cache="false"></script>
    <script src="js/markdown/demo.js" cache="false"></script>
    </body>
    </html>
    <?php
} else {
    echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
?>
