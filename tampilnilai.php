<?php
session_start();
if(isset($_SESSION['username']) && isset($_SESSION['level'])){
include "koneksi.php";
include "header.php";
include "navigasi.php";
include "footer.php";
?>
<section id="content"> 
<section class="vbox"> 
<section class="scrollable padder"> 
<div class="m-b-md"> 
<h3 class="m-b-none">SMAN 7 Mataram</h3> <small>Mendidik Untuk Maju</small> </div> 
<br>
<section class="panel panel-default"> 
<header class="panel-heading"> Data Nilai</header> 
<div class="table-responsive"> 
<table class="table table-striped m-b-none" > 
<thead> <tr> 
<th width="5%">No</th> 
<th width="15%">Nama Mapel</th> 
<th width="15%">Nis</th> 
<th width="15%">Nama Siswa</th> 
<th width="5%">Salah</th>
<th width="5%">Benar</th>
<th width="15%">Nilai</th>
<th width="15%">Tanggal</th>
<th width="15%">Ujian Ke</th>

</tr> </thead> 
<tbody>
<?php
	$a="select * from tbnilai,mapel,siswa where mapel.Kd_mapel=tbnilai.Kd_mapel AND siswa.Nis=tbnilai.Nis AND tbnilai.Nis='$_SESSION[username]'";
	$b=mysqli_query($koneksi,$a);
	$no=1;
	while($c=mysqli_fetch_array($b)){
	?>

<tr>
								<td><?php echo $no;?></td>
								<td><?php echo $c['Nama_mapel'];?></td>
								<td><?php echo $c['Nis'];?></td>
								<td><?php echo $c['Nama'];?></td>
								<td><?php echo $c['Salah'];?></td>
								<td><?php echo $c['Benar'];?></td>
								<td><?php echo $c['Nilai'];?></td>
								<td><?php echo $c['Tanggal'];?></td>
								<td><?php echo $c['Ujian_ke'];?></td>
								
</tr>
	  <?php $no++; } ?>


</tbody>
</table> </div> </section> 
</section> 
</section>
 </section>
 <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav">
 </a> 
 </section> 

<script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
<script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script> <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script> <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script> 
<script src="js/charts/flot/demo.js" cache="false"></script> 
<script src="js/calendar/bootstrap_calendar.js" cache="false"></script> 
<script src="js/calendar/demo.js" cache="false"></script> 
<script src="js/sortable/jquery.sortable.js" cache="false"></script>
<script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
<script src="js/fuelux/fuelux.js" cache="false"></script>
<script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script>
<script src="js/slider/bootstrap-slider.js" cache="false"></script>
<script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script>
<script src="js/libs/moment.min.js" cache="false"></script>
<script src="js/combodate/combodate.js" cache="false"></script>
<script src="js/select2/select2.min.js" cache="false"></script>
<script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
<script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
<script src="js/wysiwyg/demo.js" cache="false"></script>
<script src="js/markdown/epiceditor.min.js" cache="false"></script>
<script src="js/markdown/demo.js" cache="false"></script>
</body>
</html>
 <?php
 }else{
echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
 ?>
