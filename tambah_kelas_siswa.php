<?php
session_start();
//error_reporting(0);
if (isset($_SESSION['username']) && isset($_SESSION['level'])) {
    include "koneksi.php";
    include "header.php";
    include "navigasi.php";
    include "footer.php";
    include 'koneksi.php';
    $query = mysqli_query($koneksi, "SELECT * FROM siswa WHERE Nis NOT IN (SELECT nis FROM kelas)");
    $kelas = mysqli_query($koneksi, "SELECT * FROM nama_kelas");
    ?>
    <section id="content">
        <section class="vbox">
            <header class="header bg-white b-b b-light">
                <p>SMAN 7 Mataram
                    <small>(Mendidik Untuk Maju)</small>
                </p>
            </header>
            <section class="scrollable wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="kelas.php" class="btn btn-danger btn-sm btn-rounded"><i
                                            class="fa fa-arrow-circle-left"></i> kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
<!--                    from-->
                    <form action="" method="post">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <header class="panel-heading"><b><i class="fa fa-table"></i> Data siswa belum memiliki kelas</b></header>
                            <div class="table-responsive">
                                <table class="table table-striped b-t b-light text-sm">
                                    <thead>
                                    <tr>
                                        <th width="20"><input type="checkbox"></th>
                                        <th class="th-sortable" data-toggle="class">NIS</th>
                                        <th>NAMA SISWA</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php while ($row = mysqli_fetch_array($query)) : ?>
                                    <tr>
                                        <td><input type="checkbox" name="nis[]" value="<?= $row['Nis'] ?>"></td>
                                        <td><?= $row['Nis'] ?></td>
                                        <td><?= $row['Nama'] ?></td>
                                    </tr>
                                    <?php endwhile; ?>
                                    </tbody>
                                </table>
                            </div>
                            <footer class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <select name="id_nama_kelas" class="input-sm form-control input-s-sm inline">
                                            <?php while ($row = mysqli_fetch_array($kelas)) : ?>
                                            <option value="<?= $row['id_nama_kelas'] ?>">Kelas : <?= $row['nama_kelas'] ?></option>
                                            <?php endwhile; ?>
                                        </select>
                                        <input type="submit" name="simpan" value="Simpan" class="btn btn-sm btn-primary"/>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                    </form>

                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
    </section>
<!--proses simpan-->
    <?php
        if (isset($_POST['simpan']))
        {
            $nis = $_POST['nis'];
            $id_nama_kelas = $_POST['id_nama_kelas'];
           if (count($nis) > 0)
           {
               foreach ($nis as $key => $val)
               {
                   mysqli_query($koneksi,"INSERT INTO kelas VALUE (NULL ,'$id_nama_kelas','$val')");
               }
               echo "
                <script>
                    window.location.href ='kelas.php';
                </script>";
           }else{
               echo "
                <script> alert('Anda belum memilih satupun siswa!!');
                window.location.href ='tambah_kelas_siswa.php';
                </script>
               ";
           }
        }
    ?>
    <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App -->
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="js/charts/flot/demo.js" cache="false"></script>
    <script src="js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="js/calendar/demo.js" cache="false"></script>
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script src="js/fuelux/fuelux.js" cache="false"></script>
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script>
    <script src="js/slider/bootstrap-slider.js" cache="false"></script>
    <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script>
    <script src="js/libs/moment.min.js" cache="false"></script>
    <script src="js/combodate/combodate.js" cache="false"></script>
    <script src="js/select2/select2.min.js" cache="false"></script>
    <script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
    <script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
    <script src="js/wysiwyg/demo.js" cache="false"></script>
    <script src="js/markdown/epiceditor.min.js" cache="false"></script>
    <script src="js/markdown/demo.js" cache="false"></script>

    <?php
} else {
    echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
