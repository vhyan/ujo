<?php
session_start();
if(isset($_SESSION['username']) && isset($_SESSION['level'])){
include "koneksi.php";
include "header.php";
include "navigasi.php";
include "footer.php";
?>
<section id="content"> 
<section class="vbox"> 
<section class="scrollable padder"> 
<div class="m-b-md"> 
<h3 class="m-b-none">SMAN 7 Mataram</h3><small>Mendidik Untuk Maju</small> </div> 
<div class="col-sm-8"> 
<section class="panel panel-default"> 
<header class="panel-heading font-bold">Input Data Guru</header> 
<div class="panel-body"> 
<?php
if(isset($_POST['simpan'])){
$nip=$_POST['nip'];
$nipe=md5($_POST['nip']);
$nama=$_POST['nama'];
$tlahir=$_POST['tlahir'];
$tgllahir=$_POST['tgllahir'];
$jk=$_POST['jk'];
$agama=$_POST['agama'];
$alamat=$_POST['alamat'];
$nohp=$_POST['nohp'];
$foto=$_FILES['foto']['name'];
		if(strlen($foto)){
			if(is_uploaded_file($_FILES['foto']['name'])){
				move_uploaded_file($_FILES['foto']['name'],"images/".$foto);
			}
			mysqli_query($koneksi,"update guru set foto='$foto' where username='$_SESSION[username]'");
		}
		if (empty($nip) or empty($nama) or empty($tlahir)or empty($tgllahir)or empty($jk)or empty($agama)or empty($alamat)or empty($nohp)){
			echo "<script language='javascript'>
alert('Data yang anda masukkan belum lengkap !');
</script>";
		}else{
		$a="insert into guru values('$nip','$nama','$tlahir','$tgllahir','$jk','$agama','$alamat','$nohp','$foto')";
		$x="insert into user values('$nama','$nip','$nipe','$foto','guru')";
	    $y=mysqli_query($koneksi,$x);
		$b=mysqli_query($koneksi,$a);
		move_uploaded_file($_FILES['foto']['tmp_name'], "images/".$_FILES['foto']['name']);
			if ($b) {
				echo "<script language='javascript'>
alert('Data berhasil disimpan');
</script>";

			}
		}
	}
?>
<form class="bs-example form-horizontal" method="post" action="" enctype="multipart/form-data"> 
<div class="form-group"> 
<label class="col-lg-2 control-label">NIP</label> <div class="col-lg-10"> <input type="text" name="nip" class="form-control" placeholder="Masukkan Nomor Induk Pegawai" autofocus> </div> </div> 
<div class="form-group"> 
<label class="col-lg-2 control-label">Nama</label> <div class="col-lg-10"> <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Lengkap"> </div> </div>
<div class="form-group">
<label class="col-lg-2 control-label">Tempat Lahir</label> <div class="col-lg-10"> <input name="tlahir" type="text" class="form-control" placeholder="Masukkan Tempat Lahir"> </div> </div> 
<div class="form-group">
<label class="col-sm-2 control-label">Tanggal Lahir</label> <div class="col-lg-10"> <input name="tgllahir" class="input-sm input-s datepicker-input form-control" size="16" type="text" value="" data-date-format="dd-mm-yyyy" > </div></div>
<div class="form-group"> 
<label class="col-sm-2 control-label">Jenis Kelamin</label>
<div class="col-sm-10"> 
<select class="input-sm input-s  form-control" name="jk"> 
<option value="Laki-Laki">Laki-Laki</option> 
<option value="Perempuan">Perempuan</option> 
</select> </div> </div>
<div class="form-group"> <label class="col-sm-2 control-label">Agama</label>
<div class="col-sm-10"> 
<select class="input-sm input-s form-control" name="agama" >
<option value="Islam">Islam</option> 
<option value="Hindu">Hindu</option>
<option value="Budha">Budha</option> 
<option value="Kristen">Kristen</option>  
</select> </div> </div>
<div class="form-group"> <label class="col-sm-2 control-label">Alamat</label>
<div class="col-sm-10">  
<textarea class="form-control m-b" name="alamat" placeholder="Masukkan Alamat" rows="3"></textarea></div></div>
<div class="form-group">
<label class="col-lg-2 control-label">Nomer HP/Telpon</label> <div class="col-lg-10"> <input type="text" name="nohp" class="form-control" placeholder="Masukkan nomer hp/telpon"> </div> </div> 
<div class="form-group"> <label class="col-sm-2 control-label">Foto Profile</label> 
<div class="col-sm-10"> 
<input type="file" name="foto" accept="image/*" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s" value="avatar1.jpg">
</div> </div>
<br>
<a href="dataguru.php"><input type="button" class="btn btn-default" value="Cancel"></input></a> 
<button type="submit" name="simpan" class="btn btn-primary">Save Change</button>

</form> 
</div> </section> </div>

</section> 
</section> 
</section>
<script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
<script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script> <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script> <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script> 
<script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script> 
<script src="js/charts/flot/demo.js" cache="false"></script> 
<script src="js/calendar/bootstrap_calendar.js" cache="false"></script> 
<script src="js/calendar/demo.js" cache="false"></script> 
<script src="js/sortable/jquery.sortable.js" cache="false"></script>
<script src="js/fuelux/fuelux.js" cache="false"></script><!-- datepicker --><script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script><!-- slider --><script src="js/slider/bootstrap-slider.js" cache="false"></script><!-- file input --> <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script><!-- combodate --><script src="js/libs/moment.min.js" cache="false"></script><script src="js/combodate/combodate.js" cache="false"></script><!-- select2 --><script src="js/select2/select2.min.js" cache="false"></script><!-- wysiwyg --><script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script><script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script><script src="js/wysiwyg/demo.js" cache="false"></script><!-- markdown --><script src="js/markdown/epiceditor.min.js" cache="false"></script><script src="js/markdown/demo.js" cache="false"></script>
</body>
</html>
<?php
	}else{
echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
	?>