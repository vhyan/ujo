<?php
session_start();
if (isset($_SESSION['username']) && isset($_SESSION['level'])) {
    include "koneksi.php";
    include "header.php";
    include "navigasi.php";
    include "footer.php";
    include 'koneksi.php';
    $query = mysqli_query($koneksi, "SELECT *, nama_kelas.id_nama_kelas, COUNT('kelas.id_kelas') as jml_siswa FROM nama_kelas JOIN kelas ON nama_kelas.id_nama_kelas=kelas.id_nama_kelas GROUP BY nama_kelas.id_nama_kelas");
    ?>
    <section id="content">
        <section class="vbox">
            <header class="header bg-white b-b b-light">
                <p>SMAN 7 Mataram
                    <small>(Mendidik Untuk Maju)</small>
                </p>
            </header>
            <section class="scrollable wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="tambah_kelas_siswa.php" class="btn btn-primary btn-rounded"><i class="fa fa-plus-circle"></i> Tambah Mahasiswa Kelas</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php while ($row = mysqli_fetch_array($query))  : ?>
                    <div class="col-lg-4">
                        <section class="panel panel-info">
                            <div class="panel-body">
                                <div class="clear">
                                    <a href="detail_kelas.php?id_nama_kelas=<?= $row['id_nama_kelas'] ?>" class="text-info"><i class="icon-twitter"></i>
                                        <h4><b>KELAS : <?= $row['nama_kelas'] ?></b></h4>
                                    </a>
                                    <small class="block text-muted">Jumlah Mahasiswa : <?= $row['jml_siswa'] ?></small>
                                </div>
                            </div>
                        </section>
                    </div>
                    <?php endwhile; ?>
                </div>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
    </section>

    <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App -->
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="js/charts/flot/demo.js" cache="false"></script>
    <script src="js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="js/calendar/demo.js" cache="false"></script>
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script src="js/fuelux/fuelux.js" cache="false"></script>
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script>
    <script src="js/slider/bootstrap-slider.js" cache="false"></script>
    <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script>
    <script src="js/libs/moment.min.js" cache="false"></script>
    <script src="js/combodate/combodate.js" cache="false"></script>
    <script src="js/select2/select2.min.js" cache="false"></script>
    <script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
    <script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
    <script src="js/wysiwyg/demo.js" cache="false"></script>
    <script src="js/markdown/epiceditor.min.js" cache="false"></script>
    <script src="js/markdown/demo.js" cache="false"></script>

    <?php
} else {
    echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
