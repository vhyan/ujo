<?php
session_start();
if (isset($_SESSION['username']) && isset($_SESSION['level'])) {
    include "koneksi.php";
    include "header.php";
    include "navigasi.php";
    include "footer.php";
    if (isset($_GET['Id_soal'])) {
        $id = $_GET['Id_soal'];
        $sql = "select * from tbsoal where Id_soal='$id'";
        $query = mysqli_query($koneksi, $sql);
        $data = mysqli_fetch_array($query);
    } else {
        echo "Data yang diubah belum ada";
    }
    ?>

    <section id="content">
        <section class="vbox">
            <section class="scrollable padder">
                <div class="m-b-md">
                    <h3 class="m-b-none">SMAN 7 Mataram</h3>
                    <small>Mendidik Untuk Maju</small>
                </div>
                <div class="col-sm-10">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Ubah Data Soal</header>
                        <div class="panel-body">
                            <?php
                            if (isset($_POST['ubah'])) {
                                $kd_mapel = $_POST['kd_mapel'];
                                $soal = $_POST['text-ckeditor'];
                                $jawaban = $_POST['jawaban'];
                                $jenis_soal = $_POST['jenis_soal'];
                                $pilihan1 = $_POST['pilihan1'];
                                $pilihan2 = $_POST['pilihan2'];
                                $pilihan3 = $_POST['pilihan3'];
                                $pilihan4 = $_POST['pilihan4'];
                                if ($jenis_soal == 'G')
                                {
                                    if (empty($kd_mapel) || empty($soal) || empty($jawaban) || empty($pilihan1) || empty($pilihan2) || empty($pilihan3) || empty($pilihan4)) {
                                        echo "<script language='javascript'>
                                    alert('Data belum lengkap');
                                    document.location='datasoal.php';
                                    </script>";
                                    } else {
                                        $sql = "UPDATE tbsoal SET Kd_mapel='$kd_mapel', Soal='$soal', Jawaban='$jawaban', Pilihan1='$pilihan1',Pilihan2='$pilihan2',Pilihan3='$pilihan3',Pilihan4='$pilihan4' WHERE Id_soal='$id' ";
                                        $query = mysqli_query($koneksi, $sql) or die(mysql_error());
                                        if ($query) {
                                            echo "<script language='javascript'>
                                        alert('Ubah Data berhasil');
                                        document.location='datasoal.php';
                                        </script>";
                                        }
                                    }
                                }else{

                                    if (empty($kd_mapel) || empty($soal) || empty($jawaban)) {
                                        echo "<script language='javascript'>
                                        alert('Data belum lengkap');
                                        document.location='datasoal.php';
                                        </script>";
                                    } else {
                                        $sql = "UPDATE tbsoal SET Kd_mapel='$kd_mapel', Soal='$soal', Jawaban='$jawaban', jenis_soal='$jenis_soal' WHERE Id_soal='$id' ";
                                        $query = mysqli_query($koneksi, $sql) or die(mysql_error());
                                        if ($query) {
                                            echo "<script language='javascript'>
                                            alert('Ubah Data berhasil');
                                            document.location='datasoal.php';
                                            </script>";
                                        }
                                    }
                                }

                            }
                            ?>
                            <form class="bs-example form-horizontal" method="post" action=""
                                  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Mapel</label>
                                    <div class="col-lg-10">

                                        <select class="input-sm input-s  form-control" name="kd_mapel">
                                            <?php
                                            $mapel = "select * from mapel";
                                            $query = mysqli_query($koneksi, $mapel);
                                            if ($query === FALSE) {
                                                die (mysql_error());
                                            }
                                            while ($a = mysqli_fetch_array($query)) {
                                                echo "<option value='$a[Kd_mapel]' >$a[Nama_mapel]</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group"><label class="col-sm-2 control-label">Soal</label>
                                    <div class="col-sm-10">
                                        <textarea name="text-ckeditor" id="text-ckeditor" class="form-control m-b"
                                                  rows="3"><?php echo $data['Soal']; ?></textarea>
                                        <script>CKEDITOR.replace('text-ckeditor');</script>
                                    </div>
                                </div>

                                <div class="form-group"> <label class="col-sm-2 control-label">Jenis So'al</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="jenis_soal" id="jenis_soal">
                                            <option <?= $data['jenis_soal'] == 'G' ? 'selected' : '' ?> value="G">PILIHAN GANDA</option>
                                            <option <?= $data['jenis_soal'] == 'E' ? 'selected' : '' ?> value="E">ESAY</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-sm-2 control-label">Jawaban</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control m-b" name="jawaban"
                                                  rows="3"><?php echo $data['Jawaban']; ?></textarea>
                                    </div>
                                </div>

                                <div id="pilihan">
                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 1</label>
                                        <div class="col-sm-10">
                                        <textarea class="form-control m-b" name="pilihan1"
                                                  rows="3"><?php echo $data['Pilihan1']; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 2</label>
                                        <div class="col-sm-10">
                                        <textarea class="form-control m-b" name="pilihan2"
                                                  rows="3"><?php echo $data['Pilihan2']; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 3</label>
                                        <div class="col-sm-10">
                                        <textarea class="form-control m-b" name="pilihan3"
                                                  rows="3"><?php echo $data['Pilihan3']; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Pilihan 4</label>
                                        <div class="col-sm-10">
                                        <textarea class="form-control m-b" name="pilihan4"
                                                  rows="3"><?php echo $data['Pilihan4']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <a href='datasoal.php'><input type='button' class='btn btn-default'
                                                              value='Cancel'></input></a>
                                <button type='submit' name='ubah' class='btn btn-primary'>Ubah</button>

                            </form>
                        </div>
                    </section>
                </div>
            </section>
        </section>
    </section>

    <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App -->
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="js/charts/flot/demo.js" cache="false"></script>
    <script src="js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="js/calendar/demo.js" cache="false"></script>
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/fuelux/fuelux.js" cache="false"></script><!-- datepicker -->
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script><!-- slider -->
    <script src="js/slider/bootstrap-slider.js" cache="false"></script><!-- file input -->
    <script src="js/file-input/bootstrap-filestyle.min.js" cache="false"></script><!-- combodate -->
    <script src="js/libs/moment.min.js" cache="false"></script>
    <script src="js/combodate/combodate.js" cache="false"></script><!-- select2 -->
    <script src="js/select2/select2.min.js" cache="false"></script><!-- wysiwyg -->
    <script src="js/wysiwyg/jquery.hotkeys.js" cache="false"></script>
    <script src="js/wysiwyg/bootstrap-wysiwyg.js" cache="false"></script>
    <script src="js/wysiwyg/demo.js" cache="false"></script><!-- markdown -->
    <script src="js/markdown/epiceditor.min.js" cache="false"></script>
    <script src="js/markdown/demo.js" cache="false"></script>
    <script>
        $(document).ready(function () {
            var jenis_soal = $('#jenis_soal').val();
            if (jenis_soal == 'E')
            {
                $('#pilihan').hide();
            }else {
                $('#pilihan').show();
            }

            $('#jenis_soal').change(function () {
                if ($(this).val() == 'E')
                {
                    $('#pilihan').hide();
                }else {
                    $('#pilihan').show();
                }
            })
        })
    </script>
    </body>
    </html>
    <?php
} else {
    echo "<script language='javascript'>
alert('maaf anda tidak bisa mengakses, mohon login dulu!');
document.location='index.php';
</script>";
}
?>

